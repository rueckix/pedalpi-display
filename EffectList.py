from typing import List
import threading
from Debug import *

class EffectList:
    __effects: dict # map for id->effect
    __ids: List[str] # flat list of ids 
    __curIdx: int
    __sem : threading.RLock = None # using a re-entrant lock as semaphore

    def __init__(self):
        self.__effects = dict()
        self.__ids = []
        self.__curIdx = 0
        self.__sem = threading.RLock()

    def append(self, id, name):
        with self.__sem:
            if not self.exists(id):
                self.__effects[id] = name
                self.__ids.append(id)
            else:
                debug("EffectList:append - effect already exists " + id)

    # return True iff effect ID exists
    def exists(self, id) -> bool:
        with self.__sem:
            d = self.find(id)
            return d is not None

    # return effect 
    def find(self, id) -> (str, str):
        with self.__sem:
            return self.__effects.get(id)

    # select next effect in list
    def next(self):
        with self.__sem:
            self.__curIdx = (self.__curIdx +1) % len(self.__ids)

    # select previous effect in list
    def prev(self):
        with self.__sem:
            self.__curIdx = (self.__curIdx -1) % len(self.__ids)

    def current(self) -> (str, str):
        with self.__sem:
            if len(self.__ids) == 0:
                return (None, None)
        
            curID = self.__ids[self.__curIdx]
            return (curID, self.find(curID))
