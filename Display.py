import threading
from Debug import *


from PIL import Image, ImageDraw
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import ssd1306


class Display:
    __oled = None
    __rightControl = "-"
    __leftControl = "-"
    __toggleControl = "-"
    __effectName = "-"
    __effectParam = "-"
    __samplingRate = "-"

    __sem : threading.RLock = None # re-entrant lock as Semaphore

    def __init__(self):
        self.__sem = threading.RLock()
        
        # connect to I2C display
        serial = i2c(port=1, address=0x3C)
        self.__oled = ssd1306(serial, rotate=0)
        # TODO set font, maybe get font from the Arduino code... check for free font files
        # TODO maybe change font based on parameter to show
        # TODO: add function for getting the render size of a string to avoid duplication of code in right/center functions
        # TODO change layout to have a larger main parameter

    def setSamplingRate(self, s):
        with self.__sem:
            debug("setSamplingRate:" + s)
            self.__samplingRate = s

    def setRightControl(self, s):
        with self.__sem:
            debug("setRightControl:" + s)
            self.__rightControl = s

    def setLeftControl(self, s):
        with self.__sem:
            debug("setLeftControl:" + s)
            self.__leftControl = s

    def setToggleControl(self, s):
        with self.__sem:
            debug("setToggleControl:" + s)
            self.__toggleControl = s

    def setEffectName(self, s):
        with self.__sem:
            debug("setEffectName:" + s)
            self.__effectName = s

    def setEffectParam(self, s):
        with self.__sem:
            debug("setEffectParam:" + s)
            self.__effectParam = s

    def display(self):
        
        
        '''
        with self.__sem:
            debug("####################################")
            debug(self.__leftControl + "\t" + self.__rightControl)
            debug(self.__effectParam + "\t" + self.__effectName + "\t" + self.__samplingRate )
            debug(self.__toggleControl)
            debug("####################################")
        '''
            
        # create image buffer, avoid flickering
        img = Image.new("1", (self.__oled.width, self.__oled.height))
        draw = ImageDraw.Draw(img)

        with self.__sem:
            draw.text(self.__topLeft(), self.__leftControl, fill="white")
            draw.text(self.__topRight(self.__rightControl), self.__rightControl, fill="white")
            draw.text(self.__bottomCenter(self.__toggleControl), self.__toggleControl, fill="white")

            draw.text(self.__centerMid(self.__effectName), self.__effectName, fill="white")
            draw.text(self.__bottomLeft(self.__effectParam), self.__effectParam, fill="white")

            draw.text(self.__bottomRight(self.__samplingRate), self.__samplingRate, fill="white")

        # dump the buffer. This the the performance-bottleneck, maybe because i2c is rather slow
        # TODO: partial update / clearing blocks with fill=black, then overwrite
        self.__oled.display(img)
       

        return

    def __textSize(self, txt: str) -> (int, int):
        draw = ImageDraw.Draw(Image.new("1", (self.__oled.width, self.__oled.height)))
        return draw.textsize(txt)

    def __topLeft(self) -> (int, int):
        return (self.__left(), self.__top())

    def __bottomLeft(self, txt:str) -> (int, int):
        return (self.__left(), self.__bottom(txt))

    def __centerMid(self, txt: str) -> (int, int):
        return (self.__center(txt), self.__middle())

    def __topRight(self, txt: str) -> (int, int):
        return (self.__right(txt), self.__top())

    def __bottomRight(self, txt: str) -> (int, int):
        return (self.__right(txt), self.__bottom(txt))
    
    def __bottomCenter(self, txt: str) -> (int, int):
        return (self.__center(txt), self.__bottom(txt))

    def __topCenter(self, txt: str) -> (int, int):
        return (self.__center(txt), self.__top())
        

    # helper functions for positioning on the display
    def __bottom(self, txt:str) -> int:
        w, h =  self.__textSize(txt)
        return self.__oled.height - h

    def __top(self) -> int:
        return 0
    
    def __left(self) -> int:
        return 0

    def __right(self, txt: str) -> int:
        w, h =  self.__textSize(txt)
        
        return (self.__oled.width - w)

    def __center(self, txt: str) -> int:
        w, h =  self.__textSize(txt)
        return (self.__oled.width - w) / 2

    def __middle(self) ->int:
        return self.__oled.height / 2
