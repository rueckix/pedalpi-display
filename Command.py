from typing import List
from Debug import *

# Base class for all commands
class Command:
    command: str
    args: List[str]

    def __init__(self):
        self.command = ""
        self.args = []

    def setCommand(self, cmd: str):
        self.command = cmd
    
    def cmd(self) -> str:
        return self.command
    
    def argAppend(self, arg: str):
        self.args.append(arg)

    def argv(self) -> List[str]:
        return self.args

    def __str__(self):
        return self.command + ": " + str(self.args)

# Command received because of a button press 
class ButtonCommand(Command):
    pass

# Command received from pedal pi (connected via FIFO socket)
class PedalCommand(Command):
    pass
    