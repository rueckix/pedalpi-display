# PedalPi-Display

Adding an OLED display and effect control buttons to my pedalpi code

Check out the associated Thing at https://www.thingiverse.com/thing:3876468

# What is this for?
This code can be used in conjunction with https://gitlab.com/rueckix/pedalpi.

There are two components at play.
* PedalPi provides software-defined guitar effects.
* PedalPi-Display shows the current effect, its parameters and other useful information. 
* PedalPi-Display also includes buttons that can interact with PedalPi, e.g., to switch between different guitar effects.

# How does it work?
The pedalpi code needs to be configured to use FIFO as a communication backend. 
The pedalpi-display code will create a FIFO socket to communicated with pedalpi.

The serial communication protocol between the two components is documented at https://gitlab.com/rueckix/pedalpi.


PedalPi-Display relies on a number of python libraries that make programming with raspberry pi very easy. 
The key dependencies are:
* https://sourceforge.net/projects/raspberry-gpio-python/
* https://luma-oled.readthedocs.io/en/latest/install.html
* smbus
* PIL



