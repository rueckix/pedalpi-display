#!/usr/bin/env python
# coding=utf-8


# imports

import sys
# TODO make this a parameter
FIFOPATH = "/tmp/pedalpi.sock"

import itertools
import threading
import queue

import os
import time

# import helper classes
from Debug import *
from Display import *
from FifoIO import *
from Command import *
from EffectList import *

try:
    import RPi.GPIO as GPIO
except RuntimeError:
    print("Error importing RPi.GPIO!  This is probably because you need superuser privileges.  You can achieve this by using 'sudo' to run your script")

# Global Vars
myFifo: FifoIO = None
myEffects: EffectList = None
myDisplay: Display = None
myQ: queue.Queue = None


def leftButtonHandler(channel):
    # issue "prev" command to the queue 
    c = ButtonCommand()
    c.setCommand("prev")
    debug("left button pressed ... ")
    myQ.put(c)

def rightButtonHandler(channel):
    # issue "next" command to the queue 
    c = ButtonCommand()
    c.setCommand("next")
    debug("right button pressed ... ")
    myQ.put(c)


def processButtonCommand(cmd: ButtonCommand):
    id,_ = myEffects.current()
    debug("Procesing button command: " + cmd.cmd())
    if cmd.cmd() == "next":
        myEffects.next()

    if cmd.cmd() == "prev":
        myEffects.prev()

    
    newid,_ = myEffects.current()
    
    debug ("Switching effect to " + newid)

    # check if the ID actually changes (we may have only one effect in the queue)
    if (id != newid):
        myFifo.write("S:" + newid + "\n")

def processPedalCommand(cmd: PedalCommand):

    if cmd.cmd() == '#':
        myEffects.append(cmd.argv()[0], cmd.argv()[1])
    elif cmd.cmd() == '!':
        myDisplay.setEffectName(cmd.argv()[0])
    elif cmd.cmd() == '%':
        myDisplay.setSamplingRate(cmd.argv()[0])
    elif cmd.cmd() == '$':
        myDisplay.setEffectParam(cmd.argv()[0])
    elif cmd.cmd() == '?':
        if cmd.argv()[0] == 'R':
            myDisplay.setRightControl(cmd.argv()[1])
        elif cmd.argv()[0] == 'L':
            myDisplay.setLeftControl(cmd.argv()[1])
        elif cmd.argv()[0] == 'C':
            myDisplay.setToggleControl(cmd.argv()[1])

    return


# Refresh display at a 4 fps rate
def refreshDisplay(d: Display):
    while True:
        d.display()
        time.sleep(0.250)


def pollFIFO(fifo: FifoIO, q: queue.Queue):
    # read from FIFO and add commands to the queue
    while True:
        line = fifo.readLine().strip()
        
        if len(line) == 0:
            continue

        debug("pollFIFO(): read " + line)
        pcmd = PedalCommand()

        #parse received line
        cmd,sep,tail = line.partition(':')
        if sep != ':':
            debug("pollFIFO: Command syntax error(1) (" + line + ")")
            continue

        pcmd.setCommand(cmd)

        if cmd in ['%', '$', '!']:
            pcmd.argAppend(tail)
        elif cmd in ['?', '#']:
            # split again
            subcmd, sep, tail = tail.partition(':')
            if sep != ':':
                debug("pollFIFO: Command syntax error(2) (" + line + ")")
                continue
            
            pcmd.argAppend(subcmd)
            pcmd.argAppend(tail)
        else:
            debug("pollFIFO: Command syntax error(3) (" + line + ")")
            continue
        
        # no errors, push to event queue
        debug("pollFIFO(): pushing command: " +  str(pcmd))
        q.put(pcmd)
    

def setup():
    # declare global variables because we cannot modify them without it
    global myQ, myDisplay, myEffects, myFifo

    # create event queue, queue.Queue is synchronized, no need for extra thread safety
    
    myQ = queue.Queue()

    # create display, will be controlled from the main thread
    myDisplay = Display()

    # create effect list, will be controlled from the main thread
    myEffects = EffectList()
    
    # create Fifo, will be polled from a separate thread and written to from the main thread
    myFifo = FifoIO(FIFOPATH)
    
    # Setup GPIO event handling
    # See https://sourceforge.net/p/raspberry-gpio-python/wiki/BasicUsage/
    GPIO.setmode(GPIO.BOARD) # use board numbering
    btnPrev = 11  # users of this code will check their pin numbers
    btnNext = 13  # users of this code will check their pin numbers
    GPIO.setup(btnPrev, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(btnNext, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    # detect falling edge and debouce, will call back in a different thread
    GPIO.add_event_detect(btnPrev, GPIO.FALLING, callback=leftButtonHandler, bouncetime=300)
    GPIO.add_event_detect(btnNext, GPIO.FALLING, callback=rightButtonHandler, bouncetime=300)
    

    threading.Thread(name='FIFOPoll', target=pollFIFO, args=(myFifo, myQ)).start()
    threading.Thread(name='DisplayRefresh', target=refreshDisplay, args=(myDisplay,)).start()

def loop():
    debug("loop(): fetch from Q")
    cmd: Command = myQ.get()
    debug ("loop(): fetch command: " + str(cmd))
    if isinstance(cmd, ButtonCommand):
        processButtonCommand(cmd)
    elif isinstance(cmd, PedalCommand):
        processPedalCommand(cmd)
    else: 
        debug("loop - unknown command class")
      



if __name__ == "__main__":
    setup()
    while True: loop()
