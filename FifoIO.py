
import os
import socket
from Debug import *



class FifoIO:
    __handle: socket.socket = None
    def __init__(self, path: str):
        if os.path.exists(path):
            os.remove(path)
            
        debug("FifoIO::Creating FIFO Socket: " + path)
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.bind(path)
        sock.listen()

        
        debug("FifoIO::Opening FIFO, waiting for endpoint to connect")
        self.__handle, _ = sock.accept()

        debug("FifoIO::FIFO connected")
        

    def write(self, s: str):
        self.__handle.sendall(s.encode(encoding="ascii"))
        debug("FifoIO::FIFO write: " + s)

    def readLine(self) -> str:
        s = ""
        while True:
            c = self.__handle.recv(1).decode(encoding="ascii")
            if c == '\n':
                break
            s =  s + c

        
        if len(s) > 0:
            debug("FifoIO::FIFO read: " + s)
        return s
